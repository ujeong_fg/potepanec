module CategoriesHelper
  def variants_count_on_option(option:, taxon:)
    if params[:keyword]
      keyword = params[:keyword]
      products = Spree::Product.in_name_or_description(keyword)
      Spree::Variant.
        joins(:option_values).
        where(product_id: products.ids).
        where("spree_option_values.id": option.id).
        count
    else
      Spree::Variant.
        joins(:option_values).
        where(product_id: taxon.products.ids).
        where("spree_option_values.id": option.id).
        count
    end
  end
end
