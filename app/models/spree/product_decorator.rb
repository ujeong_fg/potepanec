Spree::Product.class_eval do
  scope :recent, -> (count) { order(available_on: :desc).limit(count) }
  scope :released, -> { where('available_on < ?', Time.zone.now) }
  scope :order_by_price_asc, -> do
    joins(master: :default_price).order("spree_prices.amount")
  end
  scope :order_by_price_desc, -> do
    joins(master: :default_price).order("spree_prices.amount desc")
  end
  scope :order_by_old_to_new, -> { order(available_on: :asc) }
  scope :order_by_new_to_old, -> { order(available_on: :desc) }
  scope :with_price_and_images, -> do
    includes(master: [:default_price, :images]).order("spree_assets.position")
  end

  def related_products
    # returns related products with taxon that has small taxonomy_id first
    Spree::Product.
      joins(:taxons).
      select("spree_products.*", Arel.sql("MIN(spree_taxons.taxonomy_id)")).
      where("spree_taxons.id": taxons.ids).
      where.not("spree_products.id": id).
      group("spree_products.id").
      order(Arel.sql("MIN(spree_taxons.taxonomy_id)")).
      distinct
  end

  def self.on_taxon(taxon)
    joins(:taxons).where("spree_taxons.id": taxon.id)
  end

  def self.order_by(sort_type)
    try("order_by_#{sort_type}")
  end
end
