module Potepan::IndexResourceActions
  extend ActiveSupport::Concern

  def load_sidebar_resources
    @taxons   = Spree::Taxon.where(depth: 0).includes(:children)
    @colors   = Spree::OptionType.find_by(presentation: "Color").option_values
    @sizes    = Spree::OptionType.find_by(presentation: "Size").option_values
    @taxon_id = params[:taxon_id] || 1
  end

  def load_variants_on_option
    if params[:option_id]
      option_id = params[:option_id]
      sort_type = params[:sort_type] || "new_to_old"
      @selected_variants = Spree::Variant.
        on_products_with_option_id(@products, option_id).
        with_price_and_images.
        order_by(sort_type)
    end
  end
end
