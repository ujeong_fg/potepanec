class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.friendly.find(params[:id])
    @variant = @product.variants.first || @product.master
    @line_item = Spree::LineItem.new
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).
      limit(MAX_NUMBER_OF_RELATED_PRODUCTS)

    if params[:taxon_id]
      @taxon = Spree::Taxon.find(params[:taxon_id])
    else
      @taxon = Spree::Taxon.first
    end
  end

  def show_ajax
    @product = Spree::Product.friendly.find(params[:id])
    @variant = @product.variants.find(params[:variant_id])
    @line_item = Spree::LineItem.new
  end

  def search
    @keywords = params[:keywords]

    if @keywords.blank?
      @products = []
    else
      @products = Spree::Product.
        where("spree_products.name LIKE :keyword",
              keyword: "%#{ActiveRecord::Base.sanitize_sql_like(@keywords)}%").
        or(Spree::Product.
          where("spree_products.description LIKE :keyword",
                keyword: "%#{ActiveRecord::Base.sanitize_sql_like(@keywords)}%")).
        includes(master: [:default_price, :images])
    end
  end
end
