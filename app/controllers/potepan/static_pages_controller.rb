class Potepan::StaticPagesController < ApplicationController
  MAX_NUMBER_OF_NEW_RELEASED_PRODUCTS = 8

  def home
    @new_products = Spree::Product.
      released.
      includes(master: [:default_price, :images]).
      recent(MAX_NUMBER_OF_NEW_RELEASED_PRODUCTS)
  end
end
