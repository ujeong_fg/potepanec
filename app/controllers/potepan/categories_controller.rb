class Potepan::CategoriesController < ApplicationController
  MAX_NUMBER_OF_DISPLAY_PRODUCTS = 9
  include Potepan::IndexResourceActions
  before_action :load_products, only: :show
  before_action :load_variants_on_option, only: :show
  before_action :load_sidebar_resources, only: :show

  def show
    if params[:page_type] == "List"
      @page_type = "List"
    else
      # default
      @page_type = "Grid"
    end
    respond_to do |format|
      format.html do
        render 'show'
      end
      format.js
    end
  end

  private

  def load_products
    @selected_taxon = Spree::Taxon.find(params[:id])
    sort_type = params[:sort_type] || "new_to_old"
    @products = Spree::Product.
      on_taxon(@selected_taxon).
      with_price_and_images.
      order_by(sort_type)
  end
end
