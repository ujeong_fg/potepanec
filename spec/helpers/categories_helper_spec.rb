require 'rails_helper'

RSpec.describe CategoriesHelper do
  include CategoriesHelper

  describe "#variants_count_on_option" do
    it "選ばれたtaxonに紐づき指定したoption_valueを持つvariantsの数を返す" do
      selected_taxon = create(:taxon)
      option_value = create(:option_value)
      other_option_value = create(:option_value)
      product = create(:product, taxons: [selected_taxon])
      other_product = create(:product)
      create_list(:variant, 5, option_values: [option_value], product: product)
      create_list(:variant, 3, option_values: [other_option_value], product: product)
      create(:variant, option_values: [option_value], product: other_product)

      expect(variants_count_on_option(option: option_value, taxon: selected_taxon)).
        to eq 5
    end
  end
end
