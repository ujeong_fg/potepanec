require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  scenario "visit diverse product-pages" do
    taxon = create(:taxon)
    product = create(:product_in_stock, taxons: [taxon])
    related_product = create(:product, taxons: [taxon])
    image = create(:image)
    image.update_columns(viewable_id: product.master.id, viewable_type: "Spree::Variant")
    image_of_related_product = create(:image)
    image_of_related_product.update_columns(viewable_id: related_product.master.id,
                                            viewable_type: "Spree::Variant")

    product_with_variant    = create(:base_product)
    variant                 = create(:variant, product: product_with_variant)
    variant2                = create(:variant, product: product_with_variant)
    variant2.option_values.first.update_columns(name: "Large", presentation: "L")
    image2 = create(:image, attachment_file_name: "thinking-dog.jpg")
    image2.update_columns(viewable_id: variant.id,
                          viewable_type: "Spree::Variant")

    product_not_backorderable = create(:product_not_backorderable)
    product_not_backorderable.master.stock_items.first.adjust_count_on_hand(-10)

    # basic backorderable item
    visit potepan_product_path(product)
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_content product.name
    expect(page).to have_content product.master.display_price
    expect(page).to have_content product.description
    expect(page).not_to have_css "select#select_variants_option"

    within ".item" do
      expect(find('img')[:src]).to eq "#{image.attachment.url(:large)}"
    end

    within ".thumb" do
      expect(find('img')[:src]).to eq "#{image.attachment.url(:small)}"
    end

    within ".related_product" do
      expect(find('img')[:src]).to eq "#{image_of_related_product.attachment.url(:product)}"
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.master.display_price
    end
    expect(page).to have_select "line_item_quantity", options: ('1'..'10').to_a
    product.master.stock_items.first.adjust_count_on_hand(-10)
    visit current_path
    expect(page).to have_select "line_item_quantity", options: ('1'..'10').to_a
    expect(page).to have_content "カートへ入れる"

    # product with variants(with image)
    visit potepan_product_path(product_with_variant)
    expect(page).to have_select "select_variants_option",
                                options: ["#{variant.options_text}", "#{variant2.options_text}"]
    within ".item" do
      expect(find('img')[:src]).to eq "#{image2.attachment.url(:large)}"
    end

    within ".thumb" do
      expect(find('img')[:src]).to eq "#{image2.attachment.url(:small)}"
    end

    # restock a not backorderable item
    visit potepan_product_path(product_not_backorderable)
    expect(page).to have_content "在庫なし"
    product_not_backorderable.master.stock_items.first.adjust_count_on_hand(5)
    visit current_path
    expect(page).to have_select "line_item_quantity", options: ('1'..'5').to_a
    expect(page).to have_content "カートへ入れる"

    # search with diverse keywords
    product_with_keyword = create(:product, name: "Ruby-T")
    image_of_product_with_keyword = create(:image)
    image_of_product_with_keyword.
      update_columns(viewable_id: product_with_keyword.master.id, viewable_type: "Spree::Variant")
    visit search_potepan_products_path(keywords: 'Ruby')
    expect(page).to have_content("Ruby-T")
    visit search_potepan_products_path(keywords: '')
    expect(page).to have_content("No results")
  end
end
