require 'rails_helper'

RSpec.feature "Potepan::StaticPages", type: :feature do
  scenario "visit home-page" do
    product = create(:product_in_stock)
    image = create(:image)
    image.update_columns(viewable_id: product.master.id, viewable_type: "Spree::Variant")

    visit potepan_root_path

    within ".newProducts" do
      expect(find('img')[:src]).to eq "#{image.attachment.url(:product)}"
      expect(page).to have_content product.name
      expect(page).to have_content product.master.display_price
    end
  end
end
