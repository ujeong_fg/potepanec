require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let!(:size_option_type) { create(:option_type, presentation: "Size") }

  scenario "visit category page, jump to product page, switch view mode" do
    taxonomy                 = create(:taxonomy, name: "Categories")
    other_taxonomy           = create(:taxonomy)
    root_taxon               = taxonomy.root
    other_root_taxon         = other_taxonomy.root
    leaf_taxon_with_a_child  = create(:taxon, taxonomy:  taxonomy,
                                              parent_id: root_taxon.id,
                                              name:      "Clothing")
    leaf_taxon               = create(:taxon, taxonomy:  taxonomy,
                                              parent_id: leaf_taxon_with_a_child.id,
                                              name:      "T-shirts")
    color_option_type        = create(:option_type, presentation: "Color")
    red_option_value         = create(:option_value, name: "Red", option_type: color_option_type)
    product                  = create(:product, taxons: [leaf_taxon])
    red_variant              = create(:variant, product: product, option_values: [red_option_value])
    other_product            = create(:product, taxons: [other_root_taxon])
    image                    = create(:image)
    other_image              = create(:image)
    red_variants_image       = create(:image)
    image.update_columns(viewable_id: product.master.id, viewable_type: "Spree::Variant")
    other_image.update_columns(viewable_id: other_product.master.id,
                               viewable_type: "Spree::Variant")
    red_variants_image.update_columns(viewable_id: red_variant.id,
                                      viewable_type: "Spree::Variant")

    visit potepan_category_path(other_root_taxon.id)
    expect(page).to have_title "Categories - BIGBAG Store"

    within ".page-title" do
      expect(page).to have_selector "h2", text: other_root_taxon.name
    end

    within ".breadcrumb" do
      expect(page).to have_content other_root_taxon.permalink
    end

    within "#sidebar" do
      expect(page).to have_selector "a[data-toggle=collapse]", text: root_taxon.name
      expect(page).to have_selector "a[data-toggle=collapse]", text: leaf_taxon_with_a_child.name
      expect(page).to have_link "#{leaf_taxon.name} (#{leaf_taxon.products.count})"
    end

    # test for changing displayed-products
    within ".products" do
      expect(find('img')[:src]).to eq "#{other_image.attachment.url(:product)}"
      expect(find('img')[:src]).not_to eq "#{image.attachment.url(:product)}"
    end
    click_on "#{leaf_taxon.name} (#{leaf_taxon.products.count})"

    within ".products" do
      expect(find('img')[:src]).to eq "#{image.attachment.url(:product)}"
      expect(find('img')[:src]).not_to eq "#{other_image.attachment.url(:product)}"
    end

    # test that user can back to the page where user came from
    click_on "#{product.name}"
    expect(current_path).to eq potepan_product_path(product)
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(leaf_taxon.id)

    # test for switching view modes
    expect(page).not_to have_content product.description
    expect(page).to have_css '.active.grid'
    expect(page).not_to have_css '.active.list'
    click_on "List"
    expect(page).to have_content product.description
    expect(page).not_to have_css '.active.grid'
    expect(page).to have_css '.active.list'
    click_on "Grid"
    expect(page).not_to have_content product.description
    expect(page).to have_css '.active.grid'
    expect(page).not_to have_css '.active.list'

    # 色での絞り込みのテスト
    click_on "Red"

    within ".products" do
      expect(find('img')[:src]).to eq "#{red_variants_image.attachment.url(:product)}"
      expect(find('img')[:src]).not_to eq "#{image.attachment.url(:product)}"
    end
  end
end
