require 'rails_helper'

RSpec.describe Spree::Variant, type: :model do
  describe "並び替えのためのスコープ" do
    before do
      @newest_cheap_product = create(:product, available_on: Time.now, price: 50)
      @new_most_expensive_product = create(:product, available_on: 2.weeks.ago, price: 100)
      @oldest_expensive_product = create(:product, available_on: 4.weeks.ago, price: 80)
      @old_most_cheapest_product = create(:product, available_on: 3.weeks.ago, price: 30)
    end

    context ":order_by_price_asc" do
      it "値段が安い順に返す" do
        expect(Spree::Variant.order_by_price_asc).to eq [
          @old_most_cheapest_product.master,
          @newest_cheap_product.master,
          @oldest_expensive_product.master,
          @new_most_expensive_product.master,
        ]
      end
    end

    context ":order_by_price_desc" do
      it "値段が高い順に返す" do
        expect(Spree::Variant.order_by_price_desc).to eq [
          @new_most_expensive_product.master,
          @oldest_expensive_product.master,
          @newest_cheap_product.master,
          @old_most_cheapest_product.master,
        ]
      end
    end

    context ":order_by_new_to_old" do
      it "available_onが新しい順に返す" do
        expect(Spree::Variant.order_by_new_to_old).to eq [
          @newest_cheap_product.master,
          @new_most_expensive_product.master,
          @old_most_cheapest_product.master,
          @oldest_expensive_product.master,
        ]
      end
    end

    context ":order_by_old_to_new" do
      it "available_onが古い順に返す" do
        expect(Spree::Variant.order_by_old_to_new).to eq [
          @oldest_expensive_product.master,
          @old_most_cheapest_product.master,
          @new_most_expensive_product.master,
          @newest_cheap_product.master,
        ]
      end
    end

    context ".order_by" do
      it "指定したscopeを使用する" do
        expect(Spree::Variant.order_by("price_desc")). to eq Spree::Variant.order_by_price_desc
      end
    end
  end
end
