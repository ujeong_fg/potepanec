require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#related_products" do
    before do
      product_taxon = create(:taxon, taxonomy_id: 2)
      product_taxon_category = create(:taxon, taxonomy_id: 1)
      other_product_taxon = create(:taxon)
      @product = create(:product, taxons: [product_taxon, product_taxon_category])
      @related_product = create(:product, taxons: [product_taxon])
      @related_product_same_category = create(:product, taxons: [product_taxon_category])
      @other_product = create(:product, taxons: [other_product_taxon])
    end

    it "returns related products" do
      expect(@product.related_products).to include @related_product,
                                                   @related_product_same_category
    end

    it "does not return unrelated products" do
      expect(@product.related_products).not_to include @other_product
    end

    it "returns related products with small taxonomy_id first" do
      expect(@product.related_products).to match [@related_product_same_category, @related_product]
    end
  end

  describe "scope" do
    describe ":recent" do
      before do
        @new_product = create(:product, available_on: Time.now)
        @old_product = create(:product, available_on: Time.current.prev_month)
      end

      it "returns new_products first" do
        expect(Spree::Product.recent(2).first).to eq @new_product
      end

      it "returnst old_product at the end" do
        expect(Spree::Product.recent(2).last).to eq @old_product
      end
    end

    describe ":released" do
      before do
        @new_product = create(:product, available_on: Time.now)
        @old_product = create(:product, available_on: Time.current.prev_month)
        @unreleased_product = create(:product, available_on: Time.current.next_month)
      end

      it "returns released products" do
        expect(Spree::Product.released).to include @new_product, @old_product
      end

      it "does not return unreleased product" do
        expect(Spree::Product.released).not_to include @unreleased_product
      end
    end

    describe "並び替えのためのスコープ" do
      before do
        @newest_cheap_product = create(:product, available_on: Time.now, price: 50)
        @new_most_expensive_product = create(:product, available_on: 2.weeks.ago, price: 100)
        @oldest_expensive_product = create(:product, available_on: 4.weeks.ago, price: 80)
        @old_most_cheapest_product = create(:product, available_on: 3.weeks.ago, price: 30)
      end

      context ":order_by_price_asc" do
        it "値段が安い順に返す" do
          expect(Spree::Product.order_by_price_asc).to eq [
            @old_most_cheapest_product,
            @newest_cheap_product,
            @oldest_expensive_product,
            @new_most_expensive_product,
          ]
        end
      end

      context ":order_by_price_desc" do
        it "値段が高い順に返す" do
          expect(Spree::Product.order_by_price_desc).to eq [
            @new_most_expensive_product,
            @oldest_expensive_product,
            @newest_cheap_product,
            @old_most_cheapest_product,
          ]
        end
      end

      context ":order_by_new_to_old" do
        it "available_onが新しい順に返す" do
          expect(Spree::Product.order_by_new_to_old).to eq [
            @newest_cheap_product,
            @new_most_expensive_product,
            @old_most_cheapest_product,
            @oldest_expensive_product,
          ]
        end
      end

      context ":order_by_old_to_new" do
        it "available_onが古い順に返す" do
          expect(Spree::Product.order_by_old_to_new).to eq [
            @oldest_expensive_product,
            @old_most_cheapest_product,
            @new_most_expensive_product,
            @newest_cheap_product,
          ]
        end
      end

      context ".order_by" do
        it "指定したscopeを使用する" do
          expect(Spree::Product.order_by("price_desc")). to eq Spree::Product.order_by_price_desc
        end
      end
    end
  end
end
