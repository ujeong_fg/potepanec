require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:selected_taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [selected_taxon]) }
    let(:other_product) { create(:product) }
    let(:root_taxon) { create(:taxon) }
    let(:leaf_taxon) { create(:taxon, parent_id: root_taxon.id) }
    let!(:option_color) { create(:option_type, presentation: "Color") }
    let!(:option_size) { create(:option_type, presentation: "Size") }
    let!(:red) { create(:option_value, presentation: "Red", option_type: option_color) }
    let!(:blue) { create(:option_value, presentation: "Blue", option_type: option_color) }
    let!(:products_red_variant) { create(:variant, option_values: [red], product: product) }
    let!(:products_blue_variant) { create(:variant, option_values: [blue], product: product) }
    let!(:other_products_red_variant) do
      create(:variant, option_values: [red], product: other_product)
    end

    context "カラーで絞り込みをした時" do
      before do
        get :show, params: { id: selected_taxon.id, option_id: red.id }
      end

      it "選んだtaxonに紐づく商品の内、選んだカラーの商品を@selected_variantsに割り当てる" do
        expect(assigns(:selected_variants)).to eq [products_red_variant]
      end

      it "選んだtaxonに紐づく商品の内、別のカラーの商品は@selected_variantsに割り当てない" do
        expect(assigns(:selected_variants)).not_to include products_blue_variant
      end

      it "選んだtaxonに紐付いていない商品は同じ色でも@selected_variantsに割り当てない" do
        expect(assigns(:selected_variants)).not_to include other_products_red_variant
      end
    end

    context "by grid view" do
      before do
        get :show, params: { id: selected_taxon.id }
      end

      it "reponds successfully" do
        expect(response).to be_successful
      end

      it "renders collect template" do
        expect(response).to render_template :show
      end

      it 'assigns @selected_taxon' do
        expect(assigns(:selected_taxon)).to eq selected_taxon
      end

      it 'assingns related product to @products' do
        expect(assigns(:products)).to include product
      end

      it 'does not assign unrelated product to @products' do
        expect(assigns(:products)).not_to include other_product
      end

      it 'assings only root taxons to @taxons' do
        expect(assigns(:taxons)).to include root_taxon
      end

      it "does not assign leaf taxons to @taxons" do
        expect(assigns(:taxons)).not_to include leaf_taxon
      end

      it 'assigns "Grid" to @page_type' do
        expect(assigns(:page_type)).to eq "Grid"
      end
    end

    context "by list view" do
      before do
        get :show, params: { id: selected_taxon.id, page_type: "List" }
      end

      it 'assigns "List" to @page_type' do
        expect(assigns(:page_type)).to eq "List"
      end
    end
  end
end
