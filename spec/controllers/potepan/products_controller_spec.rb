require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    context "for products without variants" do
      before do
        @product = create(:product)
        get :show, params: { id: @product.id }
      end

      it "reponds successfully" do
        expect(response).to be_successful
      end

      it "renders collect template" do
        expect(response).to render_template :show
      end

      it 'assigns @product' do
        expect(assigns(:product)).to eq @product
      end

      it 'assigns @product.master to @variant' do
        expect(assigns(:variant)).to eq @product.master
      end
    end

    context "for products with variants" do
      before do
        @product = create(:product)
        @variant = create(:variant, product: @product)
        get :show, params: { id: @product.id }
      end

      it 'assigns @product.variants.first to @variant' do
        expect(assigns(:variant)).to eq @product.variants.first
      end
    end

    context "for products with taxons" do
      before do
        taxon = create(:taxon, taxonomy_id: 2)
        taxon_2 = create(:taxon, taxonomy_id: 1)
        @product = create(:product, taxons: [taxon, taxon_2])
        @related_product = create(:product, taxons: [taxon])
        @related_product_same_category = create(:product, taxons: [taxon_2])
        @unrelated_product = create(:product)
        get :show, params: { id: @product.id }
      end

      it 'assigns related product to @related_products' do
        expect(assigns(:related_products)).to include @related_product,
                                                      @related_product_same_category
      end

      it 'does not assign unrelated product to @related_products' do
        expect(assigns(:related_products)).not_to include @unrelated_product
      end
    end
  end

  describe "#search" do
    let!(:product_with_keyword_in_name) { create(:product, name: "Ruby") }
    let!(:product_with_keyword_in_description) { create(:product, description: "Ruby") }
    let!(:product_with_special_word_in_name) { create(:product, name: "%Ruby%") }
    let!(:product_without_keyword) { create(:product, name: "PHP", description: "PHP") }

    context "with any words" do
      before do
        get :search, params: { keywords: "Ruby" }
      end

      it "reponds successfully" do
        expect(response).to be_successful
      end

      it "renders collect template" do
        expect(response).to render_template :search
      end

      it 'assigns @keywords' do
        expect(assigns(:keywords)).to eq "Ruby"
      end

      it 'assigns product_with_keyword_in_name to @products' do
        expect(assigns(:products)).to include product_with_keyword_in_name
      end

      it 'assigns product_with_keyword_in_description to @products' do
        expect(assigns(:products)).to include product_with_keyword_in_description
      end

      it 'does not assign product_without_keyword to @products' do
        expect(assigns(:products)).not_to include product_without_keyword
      end
    end

    context "without any words" do
      before do
        get :search, params: { keywords: "" }
      end

      it "reponds successfully" do
        expect(response).to be_successful
      end

      it "renders collect template" do
        expect(response).to render_template :search
      end

      it 'assigns [] to @products' do
        expect(assigns(:products)).to eq []
      end
    end

    context "with words that have to be sanitized" do
      before do
        get :search, params: { keywords: "%Ruby%" }
      end

      it "reponds successfully" do
        expect(response).to be_successful
      end

      it "renders collect template" do
        expect(response).to render_template :search
      end

      it 'assigns product_with_special_word_in_name to @products' do
        expect(assigns(:products)).to include product_with_special_word_in_name
      end

      it 'does not assign product_with_keyword_in_name to @products' do
        expect(assigns(:products)).not_to include product_with_keyword_in_name
      end
    end
  end
end
